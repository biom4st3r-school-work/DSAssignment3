#include "linked_list.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

// I really wish c has namespaces. prefixes are ugly
void llprint(Node* list) {
  printf("Linked List: \n[");
  int first = 1;
  Node* curr = list;
  while (curr != NULL) {
    printf(" %c %s", (first ? '\0' : ','), (char*)curr->data);
    curr = curr->child;
    first = 0;
  }
  printf(" ]\n\n");
}

void llpush(Node* list, size_t data) {
  Node* node = malloc(sizeof(Node));
  node->child = NULL;
  node->data = data;
  Node* tail = list;
  while (tail->child != NULL) {
    tail = tail->child;
  }
  tail->child = node;
}

Node* llpop(Node** list) {
   Node* toret = (*list);
  (*list) = (*list)->child;
  return toret;
}

void llfree(Node* list) {
  Node* node = llpop(&list);
  while (node != NULL) {
    free(node);
    node = llpop(&list);
  }
}

size_t llsize(Node* list) {
  int i = 0;
  while (list != NULL) {
    list = list->child;
    i+=1;
  }
  return i;
}

