#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int count, char** args) {
  Node node;
  node.data = -1;
  node.child = NULL;
  char buffer[100];
  printf("I really didn't want to do order numbers, \nso here you can refer to your order by name\nEG. Pizza, Salad, Coffee, Monster Energy(TM), etc\nOr you could still use numbers I guess\n");
  printf("Press Ctrl + c at any time to exit.\n\n");
  printf("Press Enter key to conintue.");
  getchar();
  printf("%c[2J%c[H", 27, 27); // VT100 code to clear screen and move to top left.
  printf("Type `exit` when your order is finished\n\n");
  while(1) {
    printf("What would you like? ");
    if (fgets(buffer, 99, stdin) == NULL) {
      printf("Error during fgets. Sorry about this\n");
      return -1;
    }
    unsigned long length = strlen(buffer); // maybe -1 to ignore the \n?
    if (length == 5 && strncmp(buffer, "exit", 4) == 0) {
      break;
    }
    char* newstr = malloc(length+1); // account for the \0
    strcpy(newstr, buffer);
    // for (int i = 0; i < length; i++) if (newstr[i] == '\n') newstr[i] = '\0';
    newstr[length-1]='\0';
    llpush(&node, (size_t)newstr);
  }
  Node* real_node = node.child;
  if (real_node == NULL) {
    printf("No Orders entered.\n");
    return 0;
  }
  do {
    llprint(real_node);
  } while ((real_node = real_node->child) != NULL);
  while (real_node != NULL) {
    Node* t = real_node;
    real_node = real_node->child;
    free((char*)t->data);
    free(t);
  }
  
}