#include <stddef.h>

typedef struct Node {
  struct Node *child;
  size_t data;
} Node;

void llprint(Node* list);

void llpush(Node* list, size_t data);

Node* llpop(Node** list);

void llfree(Node* list);

size_t llsize(Node* list);