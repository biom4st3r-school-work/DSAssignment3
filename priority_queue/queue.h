
typedef struct Tuple {
  int val;
  int priority;
} Tuple;

typedef struct Queue {
  Tuple* queue;
  int start;
  int len;
  int capacity;
} Queue;

Queue qinit(int size);
void qpush(Queue* q, int priority, int val);
int qpop(Queue* q);
void deinit(Queue* q);
void qprint(Queue* q);
