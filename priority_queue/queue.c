#include "queue.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


Queue qinit(int capacity) {
  Queue q;
  q.start = 0;
  q.capacity = capacity;
  q.queue = malloc(sizeof(Tuple) * capacity);
  q.len = 0;
  return q;
}

void qprint(Queue* q) {
  printf("\n[");
  for (int i = 0; i < q->len; i++) {
    Tuple* t = &q->queue[q->start+i];
    printf("%s\n\t(priority: %d, value: %d)", (i!=0 ? ", " : ""), t->priority, t->val);
  }
  printf("\n]\n");
}

void _qinsert(Queue* q, int priority, int val) {
  for (int i = 0; i < q->len; i++) {
    Tuple* t = q->queue+i;
    if (priority > t->priority) {
      memmove(q->queue+i+1, q->queue+i, (q->len-i) * sizeof(Tuple));
      t->priority = priority;
      t->val =  val;
      return;
    }
  }
  q->queue[q->len].priority = priority;
  q->queue[q->len].val = val;
}

void qpush(Queue* q, int priority, int val) {
  if (q->len == q->capacity) {
    printf("queue full, but attempted to insert\n");
    exit(-1);
    
  }
  if (q->start != 0) {
    memmove(q->queue, q->queue+q->start, sizeof(Tuple) * q->len);
    q->start = 0;
  }
  if (q->len == 0) {
    q->queue[0].priority = priority;
    q->queue[0].val = val;
    
  } else {
    _qinsert(q, priority, val);
  }
  q->len += 1;
}

int qpop(Queue* q) {
  q->start++;
  q->len--;
  return q->queue[q->start-1].val;
}

void deinit(Queue* q) {
  free(q->queue);
}
