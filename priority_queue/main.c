#include "queue.h"
#include <stdio.h>
#include <stdlib.h>


int main(int count, char** args) {
  int size;
  // Ask user for queue size
  printf("Size of queue? ");
  scanf("%d", &size);
  Queue q = qinit(size);
  // User input elements and their priority
  for (int i = 0; i < size; i++) {
    int priority, value;
    printf("Enter value: ");
    scanf("%d", &value);
    printf("Enter Priority: ");
    scanf("%d", &priority);
    qpush(&q, priority, value);
    // qprint(&q);
  }
  // Display elements with their priority
  qprint(&q);
  // Ask user for Dequeue
  getchar();
  while(q.len > 0) {
    printf("Press Enter to dequeue or `Ctrl + c` to exit");
    getchar();
    qpop(&q);
    qprint(&q);
  }
  deinit(&q);
}
